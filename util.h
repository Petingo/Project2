#include <bits/stdc++.h>
using namespace std;

string &trim(string &s);
bool isDigit(string &s);
string eliminateDecimal(string &s);

string &trim(string &s)
{
    if (s.empty())
    {
        return s;
    }

    s.erase(0, s.find_first_not_of(" "));
    s.erase(s.find_last_not_of(" ") + 1);
    return s;
}
bool isDigit(string &s)
{
    for (int i = 0; i < s.length(); i++)
    {
        if ((!isdigit(s[i])) && s[i] != '.')
        {
            return false;
        }
    }
    return true;
}
string eliminateDecimal(string &s)
{
    int len = s.length() - 1;
    while (s[len] != '.')
        len--;
    string tmp = "";
    for (int i = 0; i < len; i++)
    {
        tmp += s[len];
    }
    s = tmp;
    return s;
}