#pragma once
#ifndef BIGNUMBERBASE_H
#define BIGNUMBERBASE_H

#include <bits/stdc++.h>
using namespace std;

class BigNumberBase
{
  public:
    vector<int> number;
    // setter and getter
    void set(const string &s);
    void set(const int &n);
    string getString() const;
    int length() const;

    void clearFrontZero();

    // constructor
    BigNumberBase() = default;
    BigNumberBase(const string &s);
    BigNumberBase(const int &n);

    // check if it's 10^x
    bool isE() const;
    bool isZero() const;
    bool operator<(const BigNumberBase &b) const;
    bool operator>(const BigNumberBase &b) const;
    bool operator==(const BigNumberBase &b) const;
    bool operator!=(const BigNumberBase &b) const;

    BigNumberBase &operator+=(const BigNumberBase &b);
    BigNumberBase &operator-=(const BigNumberBase &b);
    BigNumberBase &operator*=(const BigNumberBase &b);

    friend BigNumberBase operator+(const BigNumberBase &, const BigNumberBase &);
    friend BigNumberBase operator-(const BigNumberBase &, const BigNumberBase &);
    friend BigNumberBase operator*(const BigNumberBase &, const BigNumberBase &);
    friend string operator/(const BigNumberBase &, const BigNumberBase &);
    friend BigNumberBase operator%(const BigNumberBase &, const BigNumberBase &);
};

#endif