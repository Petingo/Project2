#include <bits/stdc++.h>
#include "util.h"
#include "postfix.h"
#include "BigNumber.h"
using namespace std;

BigNumber calc(string &infix);
void process(string command);

set<string> variables;
map<string, BigNumber> variableTable;
BigNumber calc(string &infix)
{
    vector<string> postfix = infixToPostfix(infix);
    /* for (string &i : postfix)
        cout << i << " ";
    cout << endl; */
    stack<BigNumber> storage;
    BigNumber A, B, temp, empty("7777777");
    empty.isDecimal = false;
    for (string &i : postfix)
    {
        if (isOperator(i[0]) && i[0] != '!')
        {
            if (storage.size() < 2 && i[0] != '!')
            {
                cout << "invalid formula !!!" << endl;
                return empty;
            }
            else
            {
                B = storage.top();
                storage.pop();
                A = storage.top();
                storage.pop();

                switch (i[0])
                {
                case '+':
                    temp = A + B;
                    break;
                case '-':
                    temp = A - B;
                    break;
                case '*':
                    temp = A * B;
                    break;
                case '/':
                    temp = A / B;
                    break;
                case '^':
                    temp = A ^ B;
                    break;
                }
                storage.push(temp);
            }
        }
        else if (i[0] == '!')
        {
            if (storage.empty())
            {
                cout << "invalid formula !!!" << endl;
                return empty;
            }
            else
            {
                A = storage.top();
                storage.pop();
                temp = fact(A);
                storage.push(temp);
            }
        }
        else if (isDigit(i))
        {
            temp = BigNumber(i);
            /* for (int k = 0; k < i.length(); k++)
            {
                if (i[k] == '.')
                {
                    temp.isDecimal = true;
                    break;
                }
                else
                {
                    temp.isDecimal = false;
                }
            } */
            if (find(i.begin(), i.end(), '.') != i.end())
                temp.isDecimal = true;
            else
                temp.isDecimal = false;
            storage.push(temp);
        }
        else
        {
            if (variables.find(i) != variables.end())
            {
                storage.push(variableTable[i]);
            }
            else
            {
                cout << "variable not declared !!!" << endl;
                return empty;
            }
        }
    }
    temp = storage.top();
    return temp;
}

void process(string command)
{
    string front = "", formula = "";
    bool isSetting = false;

    // ...front... = formula
    // ex. Set Integer A = 3 + 5
    //     => front   = "Set Integer A "
    //     => formula = " 3 + 5"
    for (char c : command)
    {
        if (isSetting)
        {
            // 兩個 =
            if (c == '=')
            {
                cout << "invalid command" << endl;
                return;
            }
            formula += c;
        }
        else if (c != '=')
        {
            front += c;
        }

        if (c == '=')
        {
            isSetting = true;
        }
    }
    trim(front);
    stringstream ss(front);
    string tmp, type, varName;
    ss >> tmp >> type >> varName;
    if (tmp == "Set")
    {
        if (type == "Integer")
        {
            // TODO : here should check if tmp is valid
            BigNumber t = calc(formula);
            t.isDecimal = false;
            t.eliminateDecimal();
            variables.insert(varName);
            variableTable[varName] = t;
        }
        else if (type == "Decimal")
        {
            // TODO : here should check if tmp is valid
            BigNumber t = calc(formula);
            t.isDecimal = true;
            variables.insert(varName);
            variableTable[varName] = t;
        }
        else
        {
            cout << "invalid command" << endl;
        }
    }
    else
    {
        if (isSetting)
        {
            BigNumber t = calc(formula);
            t.isDecimal = variableTable[tmp].isDecimal;
            if (!variableTable[tmp].isDecimal)
            {
                t.eliminateDecimal();
            }
            variableTable[tmp] = t;
        }
        else
        {
            cout << calc(front).getOutput() << endl;
        }
    }
}