#include "BigNumber.h"
#include <bits/stdc++.h>
using namespace std;

void BigNumber::setZero()
{
    numerator.set("0");
    denominator.set("1");
}
void BigNumber::set(const string &s)
{
    if (s.length() == 0)
    {
        setZero();
    }
    unsigned i = 0;
    if (s[0] == '-')
    {
        isNegative = true;
        i = 1;
    }
    // convert s into the right format
    // 123.111 => 123111 & 1000
    string numPart = "", denoPart = "1";
    while (i < s.length() && s[i] != '.')
    {
        numPart += s[i];
        i++;
    }
    if (s[i] == '.')
    {
        i++; // discard '.'
        while (i < s.length())
        {
            numPart += s[i];
            denoPart += '0';
            i++;
        }
    }
    numerator.set(numPart);
    numerator.clearFrontZero();
    denominator.set(denoPart);
}
BigNumber::BigNumber() { setZero(); }

BigNumber::BigNumber(const string &s)
{
    set(s);
}

string BigNumber::getDenominator() const { return denominator.getString(); }

string BigNumber::getNumerator() const { return numerator.getString(); }

string BigNumber::getString() const
{
    return (isNegative ? "-" : "") + getNumerator() + " / " + getDenominator();
}

string BigNumber::getDecimal() const
{
    string s = "";
    if (isNegative)
    {
        s += '-';
    }
    s += numerator / denominator;
    return s;
}
string BigNumber::getInteger() const
{
    string s = "";
    if (isNegative)
    {
        s += '-';
    }
    s += numerator / denominator;
    string integer = "";
    for (int i = 0; i < s.length() && s[i] != '.'; i++)
    {
        integer += s[i];
    }
    return integer;
}
string BigNumber::getOutput() const
{
    if (isDecimal)
    {
        return getDecimal();
    }
    else
    {
        return getInteger();
    }
}

BigNumber operator+(const BigNumber &a, const BigNumber &b)
{
    BigNumber ans;
    BigNumber tempA = a, tempB = b;
    toCD(tempA, tempB);
    // -2+(-9)
    if (a.isNegative && b.isNegative)
    {
        ans.numerator = tempA.numerator + tempB.numerator;
        ans.isNegative = true;
    }
    else if (a.isNegative && !b.isNegative)
    {
        // -9+2
        if (tempA.numerator > tempB.numerator)
        {
            ans.numerator = tempA.numerator - tempB.numerator;
            ans.isNegative = true;
        }
        // -2+9 or -2+2
        else
        {
            ans.numerator = tempB.numerator - tempA.numerator;
        }
    }
    else if (!a.isNegative && b.isNegative)
    {
        // 9+(-2) or 2+(-2)
        if (tempA.numerator > tempB.numerator || tempA.numerator == tempB.numerator)
        {
            ans.numerator = tempA.numerator - tempB.numerator;
        }
        // 2+(-9)
        else
        {
            ans.numerator = tempB.numerator - tempA.numerator;
            ans.isNegative = true;
        }
    }
    // 9+2
    else
    {
        ans.numerator = tempA.numerator + tempB.numerator;
    }
    ans.denominator = tempA.denominator;
    ans.isDecimal = a.isDecimal | b.isDecimal;
    return ans;
}

BigNumber operator-(const BigNumber &a, const BigNumber &b)
{
    BigNumber ans;
    BigNumber tempA = a, tempB = b;
    toCD(tempA, tempB);
    if (tempA.isNegative && tempB.isNegative)
    {
        // -9-(-2)
        if (tempA.numerator > tempB.numerator)
        {
            ans.numerator = tempA.numerator - tempB.numerator;
            ans.isNegative = true;
        }
        // -2-(-9) or -2-(-2)
        else if (tempA.numerator < tempB.numerator || tempA.numerator == tempB.numerator) 
        {
            ans.numerator = tempB.numerator - tempA.numerator;
        }
    }
    // 9-(-2)
    else if (!a.isNegative && b.isNegative)
    {
        ans.numerator = tempA.numerator + tempB.numerator;
    }
    // -2-9
    else if (a.isNegative && !b.isNegative)
    {
        ans.numerator = tempA.numerator + tempB.numerator;
        ans.isNegative = true;
    }
    else
    {
        // 9-2 or 2-2
        if (tempA.numerator > tempB.numerator || tempA.numerator == tempB.numerator)
        {
            ans.numerator = tempA.numerator - tempB.numerator;
        }
        // 2-9
        else
        {
            ans.numerator = tempB.numerator - tempA.numerator;
            ans.isNegative = true;
        }
    }
    ans.denominator = tempA.denominator;
    ans.isDecimal = a.isDecimal | b.isDecimal;
    return ans;
}

BigNumber operator*(const BigNumber &a, const BigNumber &b)
{
    BigNumber ans;
    // here may be able to speed
    if (!(a.isZero()) && !(b.isZero()))
        ans.isNegative = a.isNegative ^ b.isNegative;
    else
        ans.isNegative = false;
    ans.numerator = a.numerator * b.numerator;
    ans.denominator = a.denominator * b.denominator;
    if (!(a.isDecimal) && !(b.isDecimal))
        ans.eliminateDecimal();
    ans.isDecimal = a.isDecimal | b.isDecimal;
    return ans;
}

BigNumber operator/(const BigNumber &a, const BigNumber &b)
{
    BigNumber ans;
    if (b.isZero())
    {
        cout << "division by zero is illegal !!!" << endl;
        BigNumber empty("7777777");
        empty.isDecimal = false;
        return empty;
    }
    if (!(a.isZero()))
        ans.isNegative = a.isNegative ^ b.isNegative;
    else
        ans.isNegative = false;
    ans.numerator = a.numerator * b.denominator;
    ans.denominator = a.denominator * b.numerator;
    if (!(a.isDecimal) && !(b.isDecimal))
        ans.eliminateDecimal();
    ans.isDecimal = a.isDecimal | b.isDecimal;
    return ans;
}

BigNumber operator*(const BigNumber &a, const BigNumberBase &b)
{
    BigNumber ans;
    ans.numerator = a.numerator * b;
    ans.denominator = a.denominator * b;
    return ans;
}

BigNumber operator^(BigNumber &a, BigNumber &b)
{
    b.toReductionOfFraction();
    BigNumber ans("1");
    if (b.denominator != 1)
    {
        if (b.denominator != 2)
        {
            cout << "power not divisible by 0.5 !!!" << endl;
            BigNumber empty("7777777");
            empty.isDecimal = false;
            return empty;
        }
        else
        { //2
            for (int i = 0; b.numerator > i; i++)
            {
                ans = ans * a;
            }
            ans = sqrt(ans);
        }
    }
    else
    {
        for (int i = 0; b.numerator > i; i++)
        {
            ans = ans * a;
        }
    }
    if (a.isNegative)
    {
        if (b.numerator.number[0] % 2 == 0)
        { //REALLY NOT SURE
            ans.isNegative = false;
        }
        else
        {
            ans.isNegative = true;
        }
    }
    if (b.isNegative)
    {
        BigNumber ansn(ans.denominator.getString());
        BigNumber ansntemp(ans.numerator.getString());
        ansn = ansn / ansntemp;
        if (ansn.denominator > ansn.numerator && !a.isDecimal && !b.isDecimal)
        {
            BigNumber empty;
            empty.isDecimal = false;
            empty.isNegative = false;
            return empty;
        }
        ansn.isNegative = ans.isNegative;
        ansn.isDecimal = a.isDecimal | b.isDecimal;
        return ansn;
    }
    ans.isDecimal = a.isDecimal | b.isDecimal;
    return ans;
}

BigNumber sqrt(BigNumber &a)
{
    //exception situation
    // sqrt Negative
    if (a.isNegative)
    {
        cout << "cant sqrt negative number!!" << endl;
        BigNumber ans("7777777");
        ans.isDecimal = false;
        //what should return? not sure
        return ans;
    }
    //sqrt 0
    if (a.numerator.getString() == "0" && a.denominator.getString() == "1")
    {
        BigNumber ans("0");
        return ans;
    }
    /* if (a.denominator != 1)
    {
        cout << "denominator is not 1!!!" << endl;
    } */
    //numerator and denominator do sqrt
    //* 100 ^100 first
    BigNumberBase aa(a.numerator);
    BigNumberBase bb(a.denominator);
    int nsize, dsize;
    nsize = a.numerator.length();
    dsize = a.denominator.length();
    for (int i = 0; i < 100; i++)
    {
        aa = aa * 100;
        bb = bb * 100;
    }
    reverse(aa.number.begin(), aa.number.end());
    reverse(bb.number.begin(), bb.number.end());
    int first, pos, time; //first do numerator
    if (aa.length() % 2 == 0)
    {
        first = aa.number[0] * 10 + aa.number[1];
        pos = 1;
        time = (nsize / 2) - 1 + 100;
    }
    else
    {
        first = aa.number[0];
        pos = 0;
        time = (nsize - 1) / 2 + 100;
    }
    BigNumberBase beq(first);
    int fq = sqrt(first);
    BigNumberBase fqq(fq);
    fq = pow(fq, 2);
    beq = beq - fq;
    for (int i = 0; i < time; i++)
    {
        beq = beq * 100;
        beq = beq + (aa.number[pos + 1]) * 10 + aa.number[pos + 2];
        pos = pos + 2;
        int b = 0;
        //cout << fqq.getString() << " " << beq.getString() << endl;
        while (beq > ((fqq * 20 + b) * b))
        {
            b++;
        }
        b--;
        beq = beq - ((fqq * 20 + b) * b);
        fqq = fqq * 10 + b;
    }
    //cout << fqq.getString() << endl;
    BigNumber ans(fqq.getString()); //numerator end

    int firstd, posd, timed; //do denominator
    if (bb.length() % 2 == 0)
    {
        firstd = bb.number[0] * 10 + bb.number[1];
        posd = 1;
        timed = (dsize / 2) - 1 + 100;
    }
    else
    {
        firstd = bb.number[0];
        posd = 0;
        timed = (dsize - 1) / 2 + 100;
    }
    BigNumberBase beqd(firstd);
    int fqd = sqrt(firstd);
    BigNumberBase fqqd(fqd);
    fqd = pow(fqd, 2);
    beqd = beqd - fqd;
    for (int i = 0; i < timed; i++)
    {
        beqd = beqd * 100;
        beqd = beqd + (bb.number[posd + 1]) * 10 + bb.number[posd + 2];
        posd = posd + 2;
        int c = 0;
        while (beqd > ((fqqd * 20 + c) * c))
        {
            c++;
        }
        c--;
        beqd = beqd - ((fqqd * 20 + c) * c);
        fqqd = fqqd * 10 + c;
    }
    BigNumber ansd(fqqd.getString()); //denominator end

    BigNumber ten("10"); //use for div 10
    for (int i = 0; i < 100; i++)
    {
        ans = ans / ten;
    }
    for (int i = 0; i < 100; i++)
    {
        ansd = ansd / ten;
    }

    ans = ans / ansd;
    return ans;
}

BigNumber fact(BigNumber &a)
{
    a.toReductionOfFraction();
    //exception situation
    if (a.isNegative)
    {
        cout << "negative factorial !!!" << endl;
        BigNumber ans("7777777");
        ans.isDecimal = false;
        // what should return? not sure
        return ans;
    }
    if (a.denominator != 1)
    {
        cout << "denominator is not 1 !!!" << endl;
        // maybe need to turn it into int?
        BigNumber ans("7777777");
        ans.isDecimal = false;
        return ans;
    }
    // 0!
    if (a.numerator.getString() == "0" && a.denominator.getString() == "1")
    {
        BigNumber ans("1");
        ans.isDecimal = false;
        return ans;
    }
    else
    {                            //a > 0
        BigNumberBase result(1); //set BigNumberBase result
        //set one BigNumberBase to count
        for (BigNumberBase now(1); now < a.numerator + 1; now = now + 1)
        {
            result = result * now;
        }
        BigNumber ans(result.getString());
        ans.isDecimal = a.isDecimal;
        return ans;
    }
}

bool operator==(const BigNumber &a, const BigNumber &b)
{
    return (a.numerator == b.numerator && a.denominator == b.denominator && a.isNegative == b.isNegative);
}

ostream &operator<<(ostream &outputStream, const BigNumber &outputNumber)
{
    outputStream << outputNumber.getOutput();
    return outputStream;
}

void toCD(BigNumber &a, BigNumber &b)
{
    if (a.denominator == b.denominator)
    {
        return;
    }
    // both are
    if (a.denominator.isE() && b.denominator.isE())
    {
        int dist = abs(a.denominator.length() - b.denominator.length());
        BigNumberBase multiplier(1);
        for (int i = 0; i < dist; i++)
            multiplier *= 10;
        if (a.denominator > b.denominator)
        {
            b = b * multiplier;
        }
        else
        {
            a = a * multiplier;
        }
    }
    else
    {
        a.numerator *= b.denominator;
        b.numerator *= a.denominator;
        a.denominator = b.denominator = a.denominator * b.denominator;
    }
}
BigNumberBase BigNumber::gcd()
{
    BigNumberBase a = numerator, b = denominator, t;
    while (!b.isZero())
    {
        t = b;
        b = a % b;
        a = t;
    }
    BigNumberBase res(a.getString());
    return res;
}
void BigNumber::toReductionOfFraction()
{
    BigNumberBase d = gcd();
    string num = numerator / d;
    string den = denominator / d;
    int len = num.length() - 1;
    while (num[len] != '.')
    {
        len--;
    }
    string tmp = "";
    for (int i = 0; i < len; i++)
    {
        tmp += num[i];
    }
    numerator = BigNumberBase(tmp);
    len = den.length() - 1;
    while (den[len] != '.')
    {
        len--;
    }
    tmp = "";
    for (int i = 0; i < len; i++)
    {
        tmp += den[i];
    }
    denominator = BigNumberBase(tmp);
}
void BigNumber::eliminateDecimal()
{
    toReductionOfFraction();
    string s = getDecimal();
    int len = s.length();
    while (s[len] != '.' && len > 0)
    {
        len--;
    }
    string intPart = "";
    if (len == 0)
    {
        intPart = s;
    }
    else
    {
        for (int i = 0; i < len; i++)
        {
            intPart += s[i];
        }
    }
    (*this).set(intPart);
}

bool BigNumber::isZero() const
{
    return (numerator.getString() == "0" && denominator.getString() == "1");
}