#include "BigNumberBase.h"
#include <bits/stdc++.h>
using namespace std;

#define MAX 99999

// setter
void BigNumberBase::set(const string &s)
{
    int length = s.length();
    number.clear();
    number.resize(length);
    for (int i = 0; i < length; i++)
    {
        number[i] = s[length - i - 1] - '0';
    }
}

void BigNumberBase::set(const int &n)
{
    if (n == 0)
    {
        set("0");
        return;
    }
    int tmp = n;
    string s = "";
    while (tmp > 0)
    {
        s += char((tmp % 10) + '0');
        tmp /= 10;
    }
    reverse(s.begin(), s.end());
    set(s);
}

// getter
string BigNumberBase::getString() const
{
    string s = "";
    for (int i = length() - 1; i >= 0; i--)
    {
        s += number[i] + '0';
    }
    return s;
}

// getter
int BigNumberBase::length() const
{
    return number.size();
}

// clear the fron zero, ex 0034123 -> 34123
void BigNumberBase::clearFrontZero()
{
    int len = length();
    while (number[len - 1] == 0 && len > 1)
    {
        len--;
    }
    number.resize(len);
}

// constructor - string
BigNumberBase::BigNumberBase(const string &s)
{
    if (s.length() != 0)
    {
        set(s);
    }
}
// constructor - int
BigNumberBase::BigNumberBase(const int &n) { set(n); }

bool BigNumberBase::isE() const
{
    for (int i = 0; i < length() - 1; i++)
    {
        if (number[i] != 0)
        {
            return false;
        }
    }
    if (number[length() - 1] != 1)
    {
        return false;
    }
    return true;
}

bool BigNumberBase::isZero() const
{
    for (int i = 0; i < length(); i++)
    {
        if (number[i] != 0)
        {
            return false;
        }
    }
    return true;
}

// operators overloading
bool BigNumberBase::operator<(const BigNumberBase &b) const
{
    if (length() < b.length())
    {
        return true;
    }
    if (length() > b.length())
    {
        return false;
    }
    for (int i = length() - 1; i >= 0; i--)
    {
        if (number[i] < b.number[i])
        {
            return true;
        }
        else if (number[i] > b.number[i])
        {
            return false;
        }
    }
    return false; // not sure, just deal with warning
}

bool BigNumberBase::operator>(const BigNumberBase &b) const
{
    return b < (*this);
}

bool BigNumberBase::operator==(const BigNumberBase &b) const
{
    if (length() != b.length())
    {
        return false;
    }
    for (int i = length() - 1; i >= 0; i--)
    {
        if (number[i] != b.number[i])
        {
            return false;
        }
    }
    return true;
}
// !=
bool BigNumberBase::operator!=(const BigNumberBase &b) const
{
    return !((*this) == b);
}
// +=
BigNumberBase &BigNumberBase::operator+=(const BigNumberBase &b)
{
    (*this) = (*this) + b;
    return (*this);
}

BigNumberBase &BigNumberBase::operator-=(const BigNumberBase &b)
{
    (*this) = (*this) - b;
    return (*this);
}
BigNumberBase &BigNumberBase::operator*=(const BigNumberBase &b)
{
    (*this) = (*this) * b;
    return (*this);
}

BigNumberBase operator+(const BigNumberBase &opA, const BigNumberBase &opB)
{
    BigNumberBase ans;
    BigNumberBase a = opA, b = opB;
    if (a < b)
    {
        swap(a, b);
    }
    // here a > b;
    ans.number.clear();
    ans.number.resize(a.length() + 1);

    int i;
    for (i = 0; i < b.length(); i++)
    {
        ans.number[i] = a.number[i] + b.number[i];
    }
    while (i < a.length())
    {
        ans.number[i] = a.number[i];
        i++;
    }
    for (i = 0; i < ans.length(); i++)
    {
        if (i + 1 == ans.length() && ans.number[i] >= 10)
        {
            ans.number[i + 1] = 0;
        }
        if (ans.number[i] >= 10)
        {
            ans.number[i + 1]++;
            ans.number[i] -= 10;
        }
    }
    /* int length = ans.length();
    if (ans.number[length - 1] == 0)
    {
        ans.number.resize(length - 1);
    } */
    //cout << ans.length() << endl;
    ans.clearFrontZero();
    return ans;
}

// assume a > b
BigNumberBase operator-(const BigNumberBase &a, const BigNumberBase &b)
{
    BigNumberBase ans;
    // a > b
    ans.number.clear();
    ans.number.resize(a.length());
    int i;
    for (i = 0; i < b.length(); i++)
    {
        ans.number[i] = a.number[i] - b.number[i];
    }
    while (i < a.length())
    {
        ans.number[i] = a.number[i];
        i++;
    }
    for (i = 0; i < ans.length() - 1; i++)
    {
        if (ans.number[i] < 0)
        {
            int borrow = ceil(fabs(ans.number[i] / 10.0));
            ans.number[i + 1] -= borrow;
            ans.number[i] += borrow * 10;
        }
    }
    ans.clearFrontZero();
    return ans;
}

BigNumberBase operator*(const BigNumberBase &a, const BigNumberBase &b)
{
    if (a.isZero() || b.isZero())
    {
        return BigNumberBase("0");
    }
    BigNumberBase ans;
    ans.number.clear();
    ans.number.resize(a.length() + b.length() + 1);
    for (int i = 0; i < a.length(); i++)
    {
        for (int k = 0; k < b.length(); k++)
        {
            ans.number[i + k] += a.number[i] * b.number[k];
        }
    }
    int i = 0;
    while (i < ans.length())
    {
        if (i + 1 == ans.length() && ans.number[i] >= 10)
        {
            ans.number.resize(ans.length() + 1);
        }
        if (ans.number[i] >= 10)
        {
            ans.number[i + 1] += ans.number[i] / 10;
            ans.number[i] %= 10;
        }
        i++;
    }
    ans.clearFrontZero();
    //cout << ans.length() << endl;
    return ans;
}

string operator/(const BigNumberBase &a, const BigNumberBase &b)
{
    // calc is the variable to store the temp result of every calculation,
    // decimalPoint records where the decimal point stay for the convenience of controling precision,
    // and lastNumber means the last number in a to be used for division;
    // it equals MAX when the digits of a is used up
    BigNumberBase calc(0);
    int multi, lastNumber;
    //int decimalPoint = -1;
    int decimalPart = 0;
    string ans;
    // is a is greater than or equal to b, then we catenate the digits of a to form calc
    if (a > b || a == b)
    {
        for (int i = a.length() - 1; i >= a.length() - b.length(); i--)
            calc = calc * 10 + a.number[i];
        // we need at least b's length of digits to perform division, so lastNumber is b.length() now
        lastNumber = b.length();
        // if calc is still less than b, then give it one more digit to make sure calc > b
        if (calc < b)
        {
            calc = calc * 10 + a.number[a.length() - lastNumber - 1];
            lastNumber++;
        }
    }
    // if a is less than b, we simply use all of a's digits
    else if (a < b)
    {
        // mark lastNumber as MAX to show that we've used all digits in a
        lastNumber = MAX;
        for (int i = a.length() - 1; i >= 0; i--)
            calc = calc * 10 + a.number[i];
        // insert "0." into the string of the answer since it's less than zero for sure
        ans.push_back('0');
        ans.push_back('.');
        // write down the position of the decimal point
        //decimalPoint = 1;
        decimalPart++;
        // add a zero for the first calculation
        calc = calc * 10;
    }
    // control of how precised you want the answer to be
    //while (ans.length() - decimalPoint <= 100)
    while (decimalPart <= 100)
    {
        // at the beginning of every round of calculation, the multiplier is set to 1
        multi = 1;
        // we continue to try how big the multiplier can be as long as the product is less than calc
        // and it's less than 9
        while (((b * multi) < calc) && (multi < 9))
            multi++;
        //get one step back
        if (b * multi > calc)
            multi--;
        // we've got the answer of calculation in this round
        ans.push_back(multi + '0');
        // subtract product from calc
        calc = calc - (b * multi);
        // the condition where the digits are used up, just as mentioned above
        if (lastNumber == a.length())
        {
            // stop the iteration if the quotient is an integer
            //if (calc == 0)
            //break;
            lastNumber = MAX;
            //decimalPoint = ans.length();
            ans.push_back('.');
        }
        if (lastNumber == MAX)
        {
            calc = calc * 10;
            decimalPart++;
        }
        else
        {
            calc = calc * 10 + a.number[a.length() - lastNumber - 1];
            lastNumber++;
        }
    }
    return ans;
}

BigNumberBase operator%(const BigNumberBase &a, const BigNumberBase &b)
{
    BigNumberBase ans, calc(0);
    int multi, lastNumber;

    if (a > b || a == b)
    {
        for (int i = a.length() - 1; i >= a.length() - b.length(); i--)
            calc = calc * 10 + a.number[i];
        lastNumber = b.length();
        if (calc < b)
        {
            calc = calc * 10 + a.number[a.length() - lastNumber - 1];
            lastNumber++;
        }
        while (lastNumber <= a.length())
        {
            multi = 1;
            while (((b * multi) < calc) && (multi < 9))
                multi++;
            if (b * multi > calc)
                multi--;
            calc = calc - (b * multi);
            if (lastNumber == a.length())
            {
                ans = calc;
                return ans;
            }
            calc = calc * 10 + a.number[a.length() - lastNumber - 1];
            lastNumber++;
        }
    }
    else
    {
        ans = a;
        return ans;
    }
}