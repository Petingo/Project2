#include <bits/stdc++.h>
#include "calc.h"
using namespace std;
class Decimal : public BigNumber
{
  public:
    Decimal() : BigNumber()
    {
        isDecimal = true;
    }
    Decimal(string &s) //: BigNumber(s)
    {
        BigNumber temp = calc(s);
        this->isNegative = temp.isNegative;
        this->numerator = temp.numerator;
        this->denominator = temp.denominator;
        isDecimal = true;
    }
    Decimal(const char *c)
    {
        string s(c);
        BigNumber temp = calc(s);
        this->isNegative = temp.isNegative;
        this->numerator = temp.numerator;
        this->denominator = temp.denominator;
        isDecimal = true;
    }
    /* friend ostream& operator<<(ostream& outputStream, Decimal& outputDecimal)
    {
        outputStream << outputDecimal.getOutPut();
        return outputStream;
    } */
    friend istream& operator>>(istream& inputStream, Decimal& inputDecimal)
    {
        string formula;
        inputStream >> formula;
        inputDecimal = Decimal(formula);
        return inputStream;
    }
};

class Integer : public BigNumber
{
  public:
    Integer() : BigNumber()
    {
        isDecimal = false;
    }
    Integer(string &s) //: BigNumber(s)
    {
        BigNumber temp = calc(s);
        this->isNegative = temp.isNegative;
        this->numerator = temp.numerator;
        this->denominator = temp.denominator;
        isDecimal = false;
    }
    Integer(const char *c)
    {
        string s(c);
        BigNumber temp = calc(s);
        this->isNegative = temp.isNegative;
        this->numerator = temp.numerator;
        this->denominator = temp.denominator;
        isDecimal = false;
    }
    /* friend ostream &operator<<(ostream &outputStream, Integer &outputInteger)
    {
        outputStream << outputInteger.getOutPut();
        return outputStream;
    } */
    friend istream &operator>>(istream &inputStream, Integer &inputInteger)
    {
        string formula;
        inputStream >> formula;
        inputInteger = Integer(formula);
        return inputStream;
    }
};

int main()
{
    string formula;
    while (getline(cin, formula))
        process(formula);
    /* Integer i = "123";
    Decimal d = "123.3";
    vector<BigNumber*> nums;
    nums.push_back(&i);
    nums.push_back(&d);
    for (const auto &num : nums)
        cout << *num << endl; */
    /* Decimal test;
    while(true)
    {
        cin >> test;
        cout << test << endl;
    } */
}