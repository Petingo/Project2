#pragma once
#ifndef BIGNUMBER_H
#define BIGNUMBER_H

#include "BigNumberBase.h"
#include <bits/stdc++.h>
using namespace std;

class BigNumber
{
  public:
    bool isNegative = false;
    bool isDecimal = true;
    BigNumberBase numerator;
    BigNumberBase denominator;

    // struct looks like:
    //
    //            numerator
    // (signed) -------------
    //           denominator

    void setZero();
    void set(const string &s);
    BigNumber();
    BigNumber(const string &s);

    string getDenominator() const;
    string getNumerator() const;
    string getString() const;
    string getDecimal() const;
    string getInteger() const;
    string getOutput() const;

    friend BigNumber operator+(const BigNumber &, const BigNumber &);
    friend BigNumber operator-(const BigNumber &, const BigNumber &);
    friend BigNumber operator*(const BigNumber &, const BigNumber &);
    friend BigNumber operator/(const BigNumber &, const BigNumber &);

    // only for mutli num and deno at the same time
    friend BigNumber operator*(const BigNumber &, const BigNumberBase &);
    friend BigNumber operator^(BigNumber &, BigNumber &);
    friend bool operator==(const BigNumber &, const BigNumber &);
    friend BigNumber sqrt(BigNumber &);
    friend BigNumber fact(BigNumber &);

    friend ostream &operator<<(ostream &outputStream, const BigNumber &number);

    friend void toCD(BigNumber &a, BigNumber &b);
    BigNumberBase gcd();
    void toReductionOfFraction();
    void eliminateDecimal();
    bool isZero() const;
};

#endif