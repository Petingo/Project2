#ifndef POSTFIX_H
#define POSTFIX_H

#include <stack>
#include <string>
#include <vector>
#include <iostream>
#include <cctype>
#include <algorithm>
using namespace std;

vector<string> infixToPostfix(string infix);
int priority(string op);
bool isOperand(char test);
bool isOperator(char test);

/* int main()
{
    string infix;
    cin >> infix;
    vector<string> result;
    result = infixToPostfix(infix);
    for (int i = 0; i < result.size(); i++)
        cout << result[i] << " ";
    return 0;
} */

vector<string> infixToPostfix(string infix)
{
    vector<string> halfProduct;
    stack<char> parentheses;
    string temp = "";
    bool leftPower = false;
    infix.erase(remove(infix.begin(), infix.end(), ' '), infix.end());
    for (int i = 0; i < infix.length(); i++)
    {
        if (isOperand(infix[i]) || infix[i] == '.')
        {
            while (isOperand(infix[i]) || infix[i] == '.')
            {
                temp += infix[i];
                i++;
            }
            i--;
            halfProduct.push_back(temp);
            temp.clear();
            if (leftPower)
            {
                halfProduct.push_back(")");
                leftPower = false;
            }
        }
        else
        {
            if ((infix[i] == '-' || infix[i] == '+') && (i == 0 || infix[i - 1] == '(' || (isOperator(infix[i - 1]) && infix[i - 1] != '!')))
            {
                if (infix[i - 1] == '^')
                {
                    leftPower = true;
                    halfProduct.push_back("(");
                }
                halfProduct.push_back("(");
                halfProduct.push_back("0");
                switch (infix[i])
                {
                case '+':
                    halfProduct.push_back("+");
                    break;
                case '-':
                    halfProduct.push_back("-");
                    break;
                }
                halfProduct.push_back("1");
                halfProduct.push_back(")");
                halfProduct.push_back("*");
            }
            else
            {
                try
                {
                    if (infix[i] == '(')
                        parentheses.push('(');
                    if (infix[i] == ')')
                    {
                        if (parentheses.empty())
                            throw 0;
                        parentheses.pop();
                    }
                }
                catch (int error)
                {
                    cout << "parentheses unmatched!";
                    vector<string> empty;
                    return empty;
                }
                halfProduct.push_back(string(1, infix[i]));
            }
        }
    }

    /* for (string &i : halfProduct)
        cout << i;
    cout << endl; */

    stack<string> storage;
    vector<string> product;
    bool consecutivePower = false;
    for (int i = 0; i < halfProduct.size(); i++)
    {
        switch (halfProduct[i][0])
        {
        case '(':
            storage.push(halfProduct[i]);
            break;
        case '+':
        case '-':
        case '*':
        case '/':
        case '^':
        case '!':
            while (!storage.empty() && priority(storage.top()) >= priority(halfProduct[i]))
            {
                if (halfProduct[i] == "^" && storage.top() == "^")
                {
                    storage.push(halfProduct[i]);
                    consecutivePower = true;
                    break;
                }
                product.push_back(storage.top());
                storage.pop();
            }
            if (consecutivePower)
            {
                consecutivePower = false;
                break;
            }
            storage.push(halfProduct[i]);
            break;
        case ')':
            while (storage.top() != "(")
            {
                product.push_back(storage.top());
                storage.pop();
            }
            storage.pop();
            break;
        default:
            product.push_back(halfProduct[i]);
            break;
        }
    }
    while (!storage.empty())
    {
        product.push_back(storage.top());
        storage.pop();
    }
    return product;
}

int priority(string op)
{
    switch (op[0])
    {
    case '+':
    case '-':
        return 1;
    case '*':
    case '/':
        return 2;
    case '^':
        return 3;
    case '!':
        return 4;
    default:
        return 0;
    }
}

bool isOperand(char test)
{
    return isalpha(test) || isdigit(test);
}

bool isOperator(char test)
{
    return test == '+' || test == '-' || test == '*' || test == '/' || test == '^' || test == '!';
}

#endif